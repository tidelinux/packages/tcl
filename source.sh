# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://downloads.sourceforge.net/tcl/tcl${BPM_PKG_VERSION}-src.tar.gz"
DOWNLOAD_DOC="https://downloads.sourceforge.net/tcl/tcl${BPM_PKG_VERSION}-html.tar.gz"
FILENAME="${DOWNLOAD##*/}"
FILENAME_DOC="${DOWNLOAD_DOC##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  wget "$DOWNLOAD_DOC"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
  mkdir -p "$BPM_SOURCE"/Documentation
  tar -xvf "$FILENAME_DOC" --strip-components=1 -C "$BPM_SOURCE"/Documentation
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  SRCDIR=$(pwd)
  cd unix
  ./configure --prefix=/usr           \
              --mandir=/usr/share/man
  make

  tdbc_pkg=tdbc1.1.5

  sed -e "s|$SRCDIR/unix|/usr/lib|" \
      -e "s|$SRCDIR|/usr/include|"  \
      -i tclConfig.sh

  sed -e "s|$SRCDIR/unix/pkgs/${tdbc_pkg}|/usr/lib/${tdbc_pkg}|" \
      -e "s|$SRCDIR/pkgs/${tdbc_pkg}/generic|/usr/include|"    \
      -e "s|$SRCDIR/pkgs/${tdbc_pkg}/library|/usr/lib/tcl${BPM_PKG_VERSION%.*}|" \
      -e "s|$SRCDIR/pkgs/${tdbc_pkg}|/usr/include|"            \
      -i pkgs/${tdbc_pkg}/tdbcConfig.sh

  itcl_pkg=itcl4.2.3
  
  sed -e "s|$SRCDIR/unix/pkgs/${itcl_pkg}|/usr/lib/${itcl_pkg}|" \
      -e "s|$SRCDIR/pkgs/${itcl_pkg}/generic|/usr/include|"    \
      -e "s|$SRCDIR/pkgs/${itcl_pkg}|/usr/include|"            \
      -i pkgs/${itcl_pkg}/itclConfig.sh

  unset SRCDIR
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  cd unix
  make test
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  cd unix
  make DESTDIR="$BPM_OUTPUT" install
  chmod -v u+w "$BPM_OUTPUT"/usr/lib/libtcl8.6.so
  make DESTDIR="$BPM_OUTPUT" install-private-headers
  ln -sfv tclsh${BPM_PKG_VERSION%.*} "$BPM_OUTPUT"/usr/bin/tclsh
  mv "$BPM_OUTPUT"/usr/share/man/man3/{Thread,Tcl_Thread}.3
  mkdir -p "$BPM_OUTPUT"/usr/share/licenses/tcl/
  install -v -m644 ../license.terms "$BPM_OUTPUT"/usr/share/licenses/tcl/license.terms
  cd ../Documentation
  mkdir -v -p "$BPM_OUTPUT"/usr/share/doc/tcl
  cp -v -r  ./html/* "$BPM_OUTPUT"/usr/share/doc/tcl
}
